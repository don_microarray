analyses.mk: analysis_to_run
	echo 'ANALYSES='$$(echo 'load("analysis_to_run"); print(names(analysis.to.run),width=5)'|\
	$(R) $(ROPTS)|grep -v '^>'|\
	perl -n -e 'print map {s{\.}{_}g; qq{$$_\n}} m{"([^\"]+)"}g;') > analyses.mk

sample_information.txt: sample_information.org
	grep -v -e '---' -e '^$$' $< |perl -pe 's/^\|\s*|\s*\|$$//g; s/\ *\|\ */\t/g;' > $@;

sample_information.xls: sample_information.txt
	txt2xls $< > $@

compare_analysis.txt: compare_analysis.R significant_cut combine_analysis
	$(R) $(ROPTS) -f $<

significant_cut_summary.txt: array_to_text.R significant_cut	
	$(R) $(ROPTS) -f $< --args significant_cut significant.cuts.summary.table $@

significant_cut.txt: array_to_text.R significant_cut annotation_comp
	$(R) $(ROPTS) -f $< --args significant_cut combined.analysis.cut.array $@

significant_cut_plot.pdf: significant_cut_plot.R significant_cut
	$(R) $(ROPTS) -f $<
	ps2pdf $(patsubst %.pdf,%.ps,$@) $@
	rm -f $(patsubst %.pdf,%.ps,$@)

significant_cut_simplified.txt: array_to_text.R significant_cut_simplified
	$(R) $(ROPTS) -f $< --args significant_cut_simplified significant.cuts.simplified $@

significant_cut_simplified: simplify_significant_cut.R significant_cut
	$(R) $(ROPTS) -f $<

significant_cut: significant_cut.R combine_analysis
	$(R) $(ROPTS) -f $<

combine_analysis.txt: array_to_text.R combine_analysis
	$(R) $(ROPTS) -f $< --args combine_analysis combined.analysis.array $@

combine_analysis: combine_analysis.R  $(patsubst %,%_analysis,$(ANALYSES)) annotation_comp
	$(R) $(ROPTS) -f $< --args $(patsubst %,%_analysis,$(ANALYSES))

%_analysis_plot.pdf: %_analysis plot_analysis.R
	$(R) $(ROPTS) -f $(filter %plot_analysis.R,$^) --args $< $@

%_analysis.txt: %_analysis array_to_text.R
	$(R) $(ROPTS) -f $(filter %array_to_text.R,$^) --args $< $(subst _,.,$(patsubst %_analysis.txt,%,$@)) $@

%_analysis: run_analysis.R sample_information analysis_to_run sample_expression_mas5 
	$(R) $(ROPTS) -f $< --args $(wordlist 2,$(words $^),$^) $@

analysis_to_run: analysis_to_run.R sample_grouping
	$(R) $(ROPTS) -f $< --args $(wordlist 2,$(words $^),$^) $@

sample_grouping: sample_grouping.R sample_information
	$(R) $(ROPTS) -f $< --args $(wordlist 2,$(words $^),$^) $@

sample_expression_mas5: make_mas5_expression.R sample_information
	$(R) $(ROPTS) -f $< --args $(wordlist 2,$(words $^),$^) $@

sample_expression_mas5_normalized: make_normalized_expression.R sample_expression_mas5 sample_information
	$(R) $(ROPTS) -f $<

sample_expression_mas5_normalized_annotated: make_normalized_annotated.R annotation_comp sample_expression_mas5_normalized
	$(R) $(ROPTS) -f $<

sample_expression_mas5_normalized_annotated.txt: array_to_text.R sample_expression_mas5_normalized_annotated	
	$(R) $(ROPTS) -f $< --args sample_expression_mas5_normalized_annotated sample.expression.mas5.normalized.annotated $@


sample_expression_mva_plots.pdf: sample_expression_mva_plots.R sample_information sample_expression_mas5 sample_expression_mas5_normalized
	$(R) $(ROPTS) -f $<

annotation_comp: make_annotation.R
	$(R) $(ROPTS) -f $<

sample_information: make_sample_information.R sample_information.txt
	$(R) $(ROPTS) -f $< --args $(wordlist 2,$(words $^),$^) $@
